package fibon_tasks;

public class ThreadTask implements Runnable {
    private int num;
    private int value;
    private int prevValue;
    private int i;

    public ThreadTask(int num) {
        this.num = num;
    }

    public void run() {
        while (i <= num) {
            if (i == 0) {
                value = 0;
                System.out.println(value + " " + Thread.currentThread());
            } else if (i == 1) {
                value = 1;
                prevValue = 0;
                System.out.println(value + " " + Thread.currentThread());

            } else {
                int temp = value;
                value += prevValue;
                prevValue = temp;
                System.out.println(value + " " + Thread.currentThread());
            }
            i++;
        }
    }
}