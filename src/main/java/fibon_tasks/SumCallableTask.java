package fibon_tasks;

import java.util.concurrent.Callable;

class SumCallableTask implements Callable {
    private int num;
    private int value;
    private int prevValue;
    private int i;
    private int sum;

    public SumCallableTask(int num) {
        this.num = num;
    }

    public Integer call() {
        while(i<=num) {
            if (i== 0){
                value = 0;
            }
            else if( i == 1) {
                value = 1; prevValue=0;

            }else {
                int temp=value;
                value+=prevValue;
                prevValue=temp;
            }
            sum+=value;
            i++;
        }
        return sum;
    }
}
