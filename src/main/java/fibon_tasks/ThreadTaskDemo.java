package fibon_tasks;

public class ThreadTaskDemo {

    public static void main(String[] args) throws InterruptedException {
        Thread task1 = new Thread(new ThreadTask(10));
        task1.start(); task1.join();
        Thread task2 = new Thread(new ThreadTask(20));
        task2.start(); task2.join();
        Thread task3 = new Thread(new ThreadTask(30));
        task3.start(); task3.join();
        Thread task4 = new Thread(new ThreadTask(40));
        task4.start(); task4.join();
    }
}
