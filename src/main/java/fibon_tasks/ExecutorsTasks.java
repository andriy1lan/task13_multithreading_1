package fibon_tasks;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutionException;

public class ExecutorsTasks {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("1.Using simple executor for generating 10 fibonacci numbers");
        new DirectExecutor().execute(new ThreadTask(10));
        Executor executor = Executors.newSingleThreadExecutor();
        System.out.println("2.Using SingleThreadExecutor for generating 8 fibonacci numbers");
        executor.execute(new ThreadTask(8));
        //ExecutorService executorService = Executors.newSingleThreadExecutor();
        //executorService.submit(new ThreadTask(7));
        //executorService..shutdown();
        ExecutorService executorService1 = Executors.newFixedThreadPool(2);
        System.out.println("3.Using FixedThreadPool(2) for generating 6 and 5 fibonacci numbers");
        executorService1.submit(new ThreadTask(6));
        executorService1.submit(new ThreadTask(5));
        executorService1.shutdown();
    }
}
