package fibon_tasks;

import java.util.concurrent.Future;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutionException;

public class SummationTasks {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("Using Callable summation task directly");
        SumCallableTask calltask=new SumCallableTask(10);
        System.out.println("Sum of first 10 fibonacci numbers "+calltask.call());
        System.out.println("Using Callable summation tasks inside threadpool");
        ExecutorService pool = Executors.newFixedThreadPool(2);
        Future<Integer> future1 = pool.submit(new SumCallableTask(10));
        System.out.println("Sum of first 10 fibonacci numbers "+future1.get());
        Future<Integer> future2 = pool.submit(new SumCallableTask(8));
        System.out.println("Sum of first 8 fibonacci numbers "+future2.get());
        pool.shutdown();

    }
}
