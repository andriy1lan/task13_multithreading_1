package pingpong;

class Pong extends Thread {
    Object lock;

    public Pong (Object object) {
        this.lock=object;
    }

    public void run() {
        try {
            while(true) {
                synchronized(lock) {
                    System.out.print("pong ");
                    System.out.println();
                    Thread.sleep(700);
                    lock.notify();
                    lock.wait();
                }
            }
        }
        catch (java.lang.InterruptedException ex) {
            return;
        }
    }

}