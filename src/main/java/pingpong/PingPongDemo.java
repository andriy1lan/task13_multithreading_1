package pingpong;

public class PingPongDemo {
    public static void main (String args[]) {
        Object obj=new Object();
        Ping ping = new Ping(obj);
        Pong pong = new Pong(obj);
        ping.start();
        pong.start();
    }
}
