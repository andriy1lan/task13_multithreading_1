package pingpong;

public class Ping extends Thread {
    //static boolean side;
    Object lock;

    public Ping(Object object) {
        this.lock = object;
    }

    public void run() {
        try {
            while (true) {
                synchronized (lock) {

                    System.out.print("ping ");
                    Thread.sleep(700);
                    lock.notify();
                    lock.wait();
                }
            }
        } catch (java.lang.InterruptedException ex) {
            return;
        }
    }
}