package sleepingtask;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.Random;

class SleepingTask implements Runnable {

    Random rand=new Random();
    static int tid=0;
    int id;
    public int getTaskId() {return id;}

    public SleepingTask () {
        tid++;  id=tid;
    }

    public void run(){ System.out.print("Starting task"+id+" - ");
    int time=rand.nextInt(10)+1;
    try{
    TimeUnit.MILLISECONDS.sleep(time*1000); }
    catch (InterruptedException ie) {System.out.print("Thread was interrupted");};
    System.out.println();
    System.out.print("The task"+id+" sleeping time is "+time);
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        //int n=Integer.parseInt(args[0]);
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(5);
        for(int i=0; i<5; i++) { //n - if for command line
            SleepingTask st=new SleepingTask();  //rand.nextInt(10)+1
            ScheduledFuture<?> future = executor.schedule(st, 1, TimeUnit.SECONDS);
            long remainingDelay = future.getDelay(TimeUnit.MILLISECONDS);
            System.out.printf("Delay of Thread "+st.getTaskId()+": %sms ", remainingDelay); System.out.println();  }
        executor.shutdown();
    }
}